---
title: mapping the Cintiq stylus under a WM
date: 2021-02-24 17:24:41
---

```
#!/usr/bin/env bash

stylusid=$(xsetwacom list devices | grep STYLUS | awk '{print $7}')
eraserid=$(xsetwacom list devices | grep ERASER | awk '{print $7}')
wacomdev=$(xrandr -q | grep " connected" | cut -d ' ' -f1 | grep HDMI)

xsetwacom set $stylusid MapToOutput $wacomdev
xsetwacom set $eraserid MapToOutput $wacomdev

echo "Stylus id is $stylusid. Eraser id is $eraserid. Cintiq ready for use!"
```

Works with a Wacom Cintiq 13HD. Maybe it works for other similar devices?

And of course it only works if the Cintiq is the only monitor named "HDMI"-something. But you can always substitute `$wacomdev` for the device name you get with `xrandr -q | grep " connected" | cut -d ' ' -f1`.

It only makes sense if you don't have a GUI tool to calibrate it more easily, but since all tools I found require either Gnome or KDE, this seemed like a pretty neat solution (after I figured it out anyway). Without it the stylus is mapped to all monitors by default, apparently, and hence _all wrong_.
