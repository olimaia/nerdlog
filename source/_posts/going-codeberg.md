---
title: going codeberg
date: 2021-03-18 15:18:14
---

So I'm gonna start the process of moving my git repos to [Codeberg](https://codeberg.org). I mean, I just did, just moved them all. But since I mostly use them for site stuff together with Netlify to actually publish the sites online, there are some aditional fiddling to get it all working _without_ gitlab. 

For now, I'll mirror it all back to gitlab and let Netlify work with that. When I'm feeling braver, I'll set everything up to compile the websites on my own and then upload them to Netlify with their cli tool.

One lil' step at a time.
